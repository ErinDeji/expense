<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

// User routes

$router->get('/users', 'UserController@index');
$router->post('/users', 'UserController@store');
$router->get('/users/{user}', 'UserController@show');
$router->put('/users/{user}', 'UserController@update');
$router->patch('/users/{user}', 'UserController@update');
$router->delete('/users/{user}', 'UserController@destroy');

// Expense routes

$router->get('/expenses', 'ExpenseController@index');
$router->post('/expenses', 'ExpenseController@store');
$router->get('/expenses/{expense}', 'ExpenseController@show');
$router->put('/expenses/{expense}', 'ExpenseController@update');
$router->patch('/expenses/{expense}', 'ExpenseController@update');
$router->delete('/expenses/{expense}', 'ExpenseController@destroy');


// ExpenseGroup routes

$router->get('/expensegroups', 'ExpenseGroupController@index');
$router->post('/expensegroups', 'ExpenseGroupController@store');
$router->get('/expensegroups/{expensegroup}', 'ExpenseGroupController@show');
$router->put('/expensegroups/{expensegroup}', 'ExpenseGroupController@update');
$router->patch('/expensegroups/{expensegroup}', 'ExpenseGroupController@update');
$router->delete('/expensegroups/{expensegroup}', 'ExpenseGroupController@destroy');