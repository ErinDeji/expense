<?php

namespace App\Http\Controllers;

use App\Expense;
//traits for success and error response
use App\Traits\ApiResponsor;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

class ExpenseController extends Controller
{

    use ApiResponsor;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //UserController $user
        // $this->user = $user;
    }

    /**
     * Return full list of Authors
     *@return Illuminate\Http\Response
     */
    public function index()
    {
        $expenses = Expense::all();
        return $this->successResponse($expenses);
    }

    /**
     * Create one new Expense
     *@return Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'user_id' => 'required|min:1',
            'group_id' => 'required|min:1',
            'benefactor_name' => 'required|max:225',
            'title' => 'required|max:225',
            'amount' => 'required|min:1',
            'description' => 'min:1'
        ];
        // $percentage = array_column($rules, 'amount');


        $this->validate($request, $rules);

        $expense = Expense::create($request->all());

        return $this->successResponse($expense, Response::HTTP_CREATED);
    }

    /**
     * Obtains and show an existing one Expense
     *@return Illuminate\Http\Response
     */
    public function show($expense)
    {
        $expense = Expense::findOrFail($expense);
        return $this->successResponse($expense);
    }

    /**
     * Update an existing Expense
     *@return Illuminate\Http\Response
     */
    public function update(Request $request, $expense)
    {
        $rules = [
            'user_id' => 'min:1',
            'group_id' => 'min:1',
            'benefactor_name' => 'max:225',
            'title' => 'max:225',
            'amount' => 'min:1',
            'description' => 'min:1'
        ];

        $this->validate($request, $rules);
        $expense = Expense::findOrFail($expense);
        $expense->fill($request->all());
        if ($expense->isClean()) {
            return $this->errorResponse(
                'Value must change',
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
        $expense->save();
        return $this->successResponse($expense);
    }

    /**
     *  Delete an existing Expense with id
     *@return Illuminate\Http\Response
     */
    public function destroy($expense)
    {
        $expense = Expense::findOrFail($expense);

        $expense->delete();
        return $this->successResponse('Deleted Successfully');
    }
}
