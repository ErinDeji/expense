<?php

namespace App\Http\Controllers;

use App\User;
//traits for success and error response
use App\Traits\ApiResponsor;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

class UserController extends Controller
{
    use ApiResponsor;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Return full list of Authors
     *@return Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return $this->successResponse($users);
    }

    /**
     * Create one new User
     *@return Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'user_name' => 'required|max:225',
            'income' => 'required|min:1',
            'budget' => 'required|min:1',
            'saving' => 'required|min:1',
            'for_spending' => 'min:0'
        ];

        // $this->income = array_column($rules, 'income');
        // $this->saving = array_column($rules, 'income');

        $this->validate($request, $rules);

        $user = User::create($request->all());

        return $this->successResponse($user, Response::HTTP_CREATED);
    }

    /**
     * Obtains and show an existing one User
     *@return Illuminate\Http\Response
     */
    public function show($user)
    {
        $user = User::findOrFail($user);
        return $this->successResponse($user);
    }

    /**
     * Update an existing User
     *@return Illuminate\Http\Response
     */
    public function update(Request $request, $user)
    {
        $rules = [
            'user_name' => 'max:225',
            'income' => 'required|min:1',
            'budget' => 'required|min:1',
            'saving' => 'required|min:1',
            'for_spending' => 'min:0'
        ];

        $this->validate($request, $rules);
        $user = User::findOrFail($user);
        $user->fill($request->all());
        if ($user->isClean()) {
            return $this->errorResponse(
                'Value must change',
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
        $user->save();
        return $this->successResponse($user);
    }

    /**
     *  Delete an existing User with id
     *@return Illuminate\Http\Response
     */
    public function destroy($user)
    {
        $user = User::findOrFail($user);

        $user->delete();
        return $this->successResponse('Deleted Successfully');
    }
}