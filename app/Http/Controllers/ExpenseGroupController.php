<?php

namespace App\Http\Controllers;

use App\ExpenseGroup;
//traits for success and error response
use App\Traits\ApiResponsor;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

class ExpenseGroupController extends Controller
{

    use ApiResponsor;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Return full list of Authors
     *@return Illuminate\Http\Response
     */
    public function index()
    {
        $expenseGroups = ExpenseGroup::all();
        return $this->successResponse($expenseGroups);
    }

    /**
     * Create one new ExpenseGroup
     *@return Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'expensegroup_name' => 'required|max:225',
        ];

        $this->validate($request, $rules);

        $expenseGroup = ExpenseGroup::create($request->all());

        return $this->successResponse($expenseGroup, Response::HTTP_CREATED);
    }

    /**
     * Obtains and show an existing one ExpenseGroup
     *@return Illuminate\Http\Response
     */
    public function show($expenseGroup)
    {
        $expenseGroup = ExpenseGroup::findOrFail($expenseGroup);
        return $this->successResponse($expenseGroup);
    }

    /**
     * Update an existing ExpenseGroup
     *@return Illuminate\Http\Response
     */
    public function update(Request $request, $expenseGroup)
    {
        $rules = [
            'expensegroup_name' => 'max:255',
        ];

        $this->validate($request, $rules);
        $expenseGroup = ExpenseGroup::findOrFail($expenseGroup);
        $expenseGroup->fill($request->all());
        if ($expenseGroup->isClean()) {
            return $this->errorResponse(
                'Value must change',
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
        $expenseGroup->save();
        return $this->successResponse($expenseGroup);
    }

    /**
     *  Delete an existing ExpenseGroup with id
     *@return Illuminate\Http\Response
     */
    public function destroy($expenseGroup)
    {
        $expenseGroup = ExpenseGroup::findOrFail($expenseGroup);

        $expenseGroup->delete();
        return $this->successResponse('Deleted Successfully');
    }
}